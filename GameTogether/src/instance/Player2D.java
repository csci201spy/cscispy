package instance;

import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.glClear;

import java.awt.Point;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Player2D extends Box2D {		
	private float speed;
	private Direction direction;
	private Texture rightTexture, leftTexture;
    public static Set<Point> currentlocation;

	
	public Player2D(float x, float y, float size, String textureFile) {
		super("Player", x, y, size, textureFile);
		speed = 2;
		width = texture.getTextureWidth()*size;
		height = texture.getTextureHeight()*size;
		
		try {
			rightTexture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(Constants.defaultPlayerRightTextureLocation));
			leftTexture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(Constants.defaultPlayerLeftTextureLocation));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		texture = rightTexture;
        currentlocation = new HashSet<Point>();
        addLocation();
	}
	public float getX(){
		return x;
	}
	public float getY(){
		return y;
	}
	
	private void checkDirection(){
		if(direction == Direction.RIGHT){
			texture = rightTexture;
		} else if(direction == Direction.LEFT){
			texture = leftTexture;
		}
	}
	
	public void update() {
		boolean checked =false;
		if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
			y += speed;
			for(float i=x; i<x+width;i++){
				for(float j=y;j>y-height;j--){
					if(Game.invalidset.contains(new Point((int)i,(int)j))){
						//System.out.println("invalid");
						y-=speed; checked = true;break;
					}
				}
				if(checked)break;
			}
            addLocation();

		}
		
		if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
			y -= speed;
			for(float i=x; i<x+width;i++){
				for(float j=y;j>y-height;j--){
					if(Game.invalidset.contains(new Point((int)i,(int)j))){
						y+=speed; checked = true;break;
					}
				}
				if(checked)break;
				
			}
            addLocation();

		}
		
		if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
			x -= speed;
			direction = Direction.LEFT;
			for(float i=x; i<x+width;i++) {
				for(float j=y;j>y-height;j--) {
					if(Game.invalidset.contains(new Point((int)i,(int)j))) {
						x+=speed; checked = true;break;
					}
				}
				if(checked)break;
			}
            addLocation();

		}
		
		if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
			x += speed;
			direction = Direction.RIGHT;
			for(float i=x; i<x+width;i++) {
				for(float j=y;j>y-height;j--) {
					if(Game.invalidset.contains(new Point((int)i,(int)j))) {
						x-=speed; checked = true;break;
					}
				}
				if(checked)break;
			}
            addLocation();

		}
	}
    private void addLocation(){
        currentlocation= new HashSet<Point>();
        int tempx = (int)x;
        int tempy = (int)y;
        //	System.out.println("------------------");
        for(tempx=(int)x; tempx<x+width;tempx++){
            currentlocation.add(new Point(tempx,tempy));
            currentlocation.add(new Point(tempx,tempy-(int)height));


        }
        tempx = (int)x;
        tempy = (int)y;
        for(tempy=(int)y;tempy>y-height;tempy--){
            currentlocation.add(new Point(tempx,tempy));
            currentlocation.add(new Point(tempx+(int)width,tempy));
            //		System.out.println(tempx + " "+tempy);
        }
        //	System.out.println("------------------");
        
    }
	public void render() {
		GL11.glEnable(GL11.GL_BLEND);
		
		checkDirection();
        
		//Color.white.bind();
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture.getTextureID());
         
        GL11.glBegin(GL11.GL_QUADS);
	        GL11.glTexCoord2f(0,0);
	        GL11.glVertex2f(x, y);
	        GL11.glTexCoord2f(1,0);
	        GL11.glVertex2f(x+(texture.getTextureWidth()*size), y);
	        GL11.glTexCoord2f(1,1);
	        GL11.glVertex2f(x+(texture.getTextureWidth()*size), y-(texture.getTextureHeight()*size));
	        GL11.glTexCoord2f(0,1);
	        GL11.glVertex2f(x, y-(texture.getTextureHeight()*size));
        GL11.glEnd();
        
        GL11.glDisable(GL11.GL_BLEND);
	}
}
