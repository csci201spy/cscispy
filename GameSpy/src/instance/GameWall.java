package instance;

import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex2f;

import java.awt.Point;
import java.io.IOException;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class GameWall extends Box2D {	
	float width, height;
	
	public GameWall(float x, float y, float width, float height, float size) {
		super("Wall", x, y, size);
		this.width = width;
		this.height = height;
		for(float i=x; i<x+width;i++){
			for(float j=y;j>y-height;j--){
				Game.invalidset.add(new Point((int)i,(int)j));
			}
		}
		try {
			texture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(Constants.wallTextureLocation));
			
            System.out.println("Texture loaded: "+texture);
            System.out.println("Texture file: " + Constants.wallTextureLocation);
            System.out.println(">> Image width: "+texture.getImageWidth());
            System.out.println(">> Image height: "+texture.getImageHeight());
            System.out.println(">> Texture width: "+texture.getTextureWidth());
            System.out.println(">> Texture height: "+texture.getTextureHeight());
            System.out.println(">> Texture ID: "+texture.getTextureID());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void update() {
		
	}
	
	public void render() {
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
		
		GL11.glEnable(GL11.GL_BLEND);
		
		Color.white.bind();
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture.getTextureID());
		
		glBegin(GL_QUADS);
			GL11.glTexCoord2f(0,0);
	        GL11.glVertex2f(x, y);
	        GL11.glTexCoord2f(size,0);
	        GL11.glVertex2f(x+width, y);
	        GL11.glTexCoord2f(size,size);
	        GL11.glVertex2f(x+width, y-height);
	        GL11.glTexCoord2f(0,size);
	        GL11.glVertex2f(x, y-height);
	    glEnd();
	    
	    GL11.glDisable(GL11.GL_BLEND);
	}
}
