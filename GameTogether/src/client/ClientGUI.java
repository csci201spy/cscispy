package client;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

import player.GameUser;

public class ClientGUI extends JFrame {
	public OpeningScreenGUI openingPanel;
	public LoginScreenGUI signinPanel;
	public StartGameScreenGUI startGamePanel;
	public SkinSelectionGUI skinPanel;
	public MapSelectionGUI mapPanel;
	public int portnumber;
	public boolean loggedIn;
	public ArrayList<String> skins;
	public ArrayList<String> maps;
	public String selectedMap;
	public int mapIndex;
	
	public ClientGUI(int portnumber) {
		super("Spy");
		
		setSize(400, 300);
		setLocation(200, 200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.portnumber = portnumber;
		initializeVariables();
		createGUI();
		setVisible(true);
	}
	private void initializeVariables(){
		loggedIn = false;
		maps = new ArrayList<String>();
		maps.add("Leavey");
		maps.add("White House");
		maps.add("Kremlin");
		selectedMap = maps.get(0);
		mapIndex = 0;
		
		openingPanel = new OpeningScreenGUI(this);		
		signinPanel = new LoginScreenGUI(this);
		skinPanel = new SkinSelectionGUI(this);
		mapPanel = new MapSelectionGUI(this);
		startGamePanel = new StartGameScreenGUI(this);	
	}
	private void createGUI(){
		add(openingPanel);
		openingPanel.setVisible(true);
	}
	public void setSelectedMap(String mapName) {
		selectedMap = mapName;
	}
	public void handleServerFeedback(GameUser userback){
		if(userback.getCommand().equals(userback.loginattempt)){
			if(userback.loginsuccess){
				System.out.println("login success");
				signinPanel.setVisible(false);
				add(startGamePanel);
				startGamePanel.setVisible(true);
				this.setSize(500,400);
				loggedIn = true;
				startGamePanel.setUserLabel(userback.getName());
			}else{
				System.out.println("failed");
			}	
		}
		
		if(userback.getCommand().equals(userback.signupattempt)){
			if(userback.signupsuccess){
				System.out.println("login success");
				signinPanel.setVisible(false);
				add(startGamePanel);
				startGamePanel.setVisible(true);
				this.setSize(500,400);
				loggedIn = true;
				startGamePanel.setUserLabel(userback.getName());

				
			}else{
				System.out.println("failed");

			}
		}
	}
	public void changeOpening(){
		setSize(200, 300);

		startGamePanel.setVisible(false);
		add(openingPanel);
		openingPanel.setVisible(true);
	}
	public void changeMap(){
		startGamePanel.setVisible(false);
		add(mapPanel);
		mapPanel.setVisible(true);
	}
	public void changeSkin(){
		startGamePanel.setVisible(false);
		add(skinPanel);
		skinPanel.setVisible(true);
	}
	public void Logout(){
		startGamePanel.setVisible(false);
		add(openingPanel);
		openingPanel.setVisible(true);
	}
}
