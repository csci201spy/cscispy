package instance;


public class Constants {
	public static final int windowWidth = 1024;
	public static final int windowHeight = 600;
	
	public static final String pngFile = ".png";
	public static final String resourceFolder = "resources/";
	public static final String mapFolder = "resources/maps/";
	public static final String skinPrefix = "skin_";
	
	public static final String defaultPlayerRightTexture = "resources/skin_default_right";
	public static final String defaultPlayerLeftTexture = "resources/skin_default_left";

	public static final String defaultGuardRightTexture = "resources/skin_guard_right";
	public static final String defaultGuardLeftTexture = "resources/skin_guard_left";
	
	public static final String defaultMapLocation = "resources/maps/map.txt";
	public static final String defaultMap2Location = "resources/maps/map2.txt";
	public static final String defaultMap3Location = "resources/maps/map3.txt";
	public static final String defaultMap4Location = "resources/maps/map4.txt";
	public static final String defaultMap5Location = "resources/maps/map5.txt";

	public static final String defaultBriefcaseTexture = "resources/briefcase";
	
	public static final String defaultExitOpenTexture = "resources/exit_open";
	public static final String defaultExitClosedTexture = "resources/exit_closed";

	public static final String wallTextureLocation = "resources/wall.png";
	public static final int resumeButtonX = (Constants.windowWidth/2)-50;
	public static final int resumeButtonY = (Constants.windowHeight/2)+175;
	public static final int changeMButotnX = (Constants.windowWidth/2)-50;
	public static final int changeMButtonY = (Constants.windowHeight/2)+75; 
	public static final int changeSButtonX = (Constants.windowWidth/2)-50;
	public static final int changeSButtonY = (Constants.windowHeight/2)-25;
	public static final int changeLButtonX = (Constants.windowWidth/2)-50;
	public static final int changeLButtonY = (Constants.windowHeight/2)-125;
	public static final int pauseMenuX = Constants.resumeButtonX-50;
	public static final int pauseMenuY = Constants.resumeButtonY+50;
	public static final int gameOverX = (Constants.windowWidth/2)-200;
	public static final int gameOverY = (Constants.windowHeight/2)+150;
	public static final int restartButtonX = (Constants.windowWidth/2)-150;
	public static final int restartButtonY = (Constants.windowHeight/2)-50;
	public static final int mainMenuButtonX = (Constants.windowWidth/2)+50;
	public static final int mainMenuButtonY = (Constants.windowHeight/2)-50;
	public static final int winScreenX = (Constants.windowWidth/2)-250;
	public static final int winScreenY = (Constants.windowHeight/2)+150;
}
