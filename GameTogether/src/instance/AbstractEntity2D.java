package instance;
public abstract class AbstractEntity2D implements Entity2D {
	protected float x, y, width, height;
	protected String name;
	public float getX() { return x; }
	public float getY() { return y; }
    public void setX(float x) { this.x = x; }
    public void setY(float y) { this.y = y; }
    public float getWidth() { return width; }
    public float getHeight() { return height; }
    public String getName() { return name; }
    public void setLocation(float x, float y) {
		this.x = x;
		this.y = y;
    }
	public abstract void setUp();
	public abstract void destroy();
	public abstract void update();
	public abstract void render();
}