package client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import instance.Game;
import util.MyButton;

public class StartGameScreenGUI extends JPanel {
	private JLabel playerNameLabel;
	private JLabel mapNameLabel;
	private MyButton skinSelectButton;
	private MyButton mapSelectButton;
	private JLabel skinImageLabel;
	private String currentSkin;
	private String currentMap;
	private MyButton startButton;
	public ClientGUI parent;
	
	public StartGameScreenGUI(ClientGUI parent) {
		super();
		this.parent = parent;
		createGUI();
	}
	
	private void createGUI() {
		setLayout(new BorderLayout());
		
		currentSkin = "default";
		currentMap = "Leavey";
		
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.Y_AXIS));
		
		playerNameLabel = new JLabel("Player: Guest");
		playerNameLabel.setFont(new Font("Arial", Font.BOLD, 20));
		playerNameLabel.setAlignmentX(CENTER_ALIGNMENT);
		topPanel.add(playerNameLabel);
		
		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));
		
		skinSelectButton = new MyButton("SKIN");
		if(!parent.loggedIn) skinSelectButton.setEnabled(false);
		skinSelectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				parent.startGamePanel.setVisible(false);
				parent.add(parent.skinPanel);
				parent.skinPanel.setVisible(true);
			}
		});
		skinSelectButton.setPreferredSize(new Dimension(150, 50));
		buttonsPanel.add(Box.createGlue());
		buttonsPanel.add(skinSelectButton);
		
		mapSelectButton = new MyButton("MAP");
		mapSelectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				parent.startGamePanel.setVisible(false);
				parent.add(parent.mapPanel);
				parent.mapPanel.setVisible(true);
			}
		});
		mapSelectButton.setPreferredSize(new Dimension(150, 50));
		buttonsPanel.add(Box.createGlue());
		buttonsPanel.add(mapSelectButton);
		buttonsPanel.add(Box.createGlue());
		buttonsPanel.setAlignmentX(CENTER_ALIGNMENT);
		topPanel.add(buttonsPanel);
		
		mapNameLabel = new JLabel("Current map: Leavey Library");
		mapNameLabel.setFont(new Font("Arial", Font.ITALIC, 16));
		mapNameLabel.setAlignmentX(CENTER_ALIGNMENT);
		topPanel.add(mapNameLabel);
	
		
		ImageIcon skinIcon = new ImageIcon("resources/skin_default_left.png");
		Image skin = skinIcon.getImage();
		Image newSkin = skin.getScaledInstance(150, 250, Image.SCALE_SMOOTH);
		skinIcon = new ImageIcon(newSkin);
		skinImageLabel = new JLabel(skinIcon);
		
		
		startButton = new MyButton("START");
		startButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				startGame();
			}
		});
		
		add(topPanel, BorderLayout.NORTH);
		add(skinImageLabel, BorderLayout.CENTER);
		add(startButton, BorderLayout.SOUTH);
	}
	
	public void setSkin(String skinName) {
		currentSkin = skinName;
		
		ImageIcon myIcon = new ImageIcon("resources/skin_" + skinName + "_left.png");
		Image skin = myIcon.getImage();
		Image newSkin = skin.getScaledInstance(150, 250, Image.SCALE_SMOOTH);
		ImageIcon skinIcon = new ImageIcon(newSkin);
		skinImageLabel.setIcon(skinIcon);
	}
	
	public void setMapLabel(String mapName) {
		mapNameLabel.setText("Current map: " + mapName);
		currentMap = mapName;
	}
	
	public void setUserLabel(String username) {
		playerNameLabel.setText("Player: " + username);
	}
	
	public void enableSkins() {
		skinSelectButton.setEnabled(true);
		repaint();
		revalidate();
	}
	
	private void startGame() {
		new Game(parent, currentSkin, currentMap);
	}
}
