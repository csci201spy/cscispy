package instance;


public class Constants {
	public static final int windowWidth = 1024;
	public static final int windowHeight = 600;
	public static final String defaultPlayerRightTextureLocation = "resources/default_skin_lowres_square.png";
	public static final String defaultPlayerLeftTextureLocation = "resources/default_skin_left.png";

	public static final String defaultGuardRightTextureLocation = "resources/default_guard_skin.png";
	public static final String defaultGuardLeftTextureLocation = "resources/default_guard_skin_left.png";
	
	public static final String defaultMapLocation = "resources/maps/map.txt";
	public static final String defaultMap2Location = "resources/maps/map2.txt";
	public static final String defaultMap3Location = "resources/maps/map3.txt";
	public static final String defaultMap4Location = "resources/maps/map4.txt";
	public static final String defaultMap5Location = "resources/maps/map5.txt";

	public static final String defaultBriefcaseTextureLocation = "resources/briefcase.png";
	
	public static final String defaultExitOpenTextureLocation = "resources/exit_open.png";
	public static final String defaultExitClosedTextureLocation = "resources/exit_closed.png";

	public static final String wallTextureLocation = "resources/wall.png";
	public static final int resumeButtonX = (Constants.windowWidth/2)-50;
	public static final int resumeButtonY = (Constants.windowHeight/2)+175;
	public static final int changeMButotnX = (Constants.windowWidth/2)-50;
	public static final int changeMButtonY = (Constants.windowHeight/2)+75; 
	public static final int changeSButtonX = (Constants.windowWidth/2)-50;
	public static final int changeSButtonY = (Constants.windowHeight/2)-25;
	public static final int changeLButtonX = (Constants.windowWidth/2)-50;
	public static final int changeLButtonY = (Constants.windowHeight/2)-125;
	public static final int pauseMenuX = Constants.resumeButtonX-50;
	public static final int pauseMenuY = Constants.resumeButtonY+50;
	public static final int gameOverX = (Constants.windowWidth/2)-200;
	public static final int gameOverY = (Constants.windowHeight/2)+150;
	public static final int restartButtonX = (Constants.windowWidth/2)-150;
	public static final int restartButtonY = (Constants.windowHeight/2)-50;
	public static final int mainMenuButtonX = (Constants.windowWidth/2)+50;
	public static final int mainMenuButtonY = (Constants.windowHeight/2)-50;
	public static final int winScreenX = (Constants.windowWidth/2)-250;
	public static final int winScreenY = (Constants.windowHeight/2)+150;
}
