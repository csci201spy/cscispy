package instance;
public interface Entity2D {
	public float getX();
	public float getY();
    public void setX(float x);
    public void setY(float y);
    public float getWidth();
    public float getHeight();
    public String getName();
    public void setLocation(float x, float y);
	public void setUp();
	public void destroy();
	public void update();
	public void render();
}