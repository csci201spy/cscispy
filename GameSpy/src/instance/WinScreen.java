package instance;

import java.io.IOException;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import client.ClientGUI;

public class WinScreen {
	private static Texture restartTexture, menuTexture, restartDownTexture, menuDownTexture, winScreen;
	private ButtonClass restartButton, menuButton, restartDownButton, menuDownButton;
	private boolean restartTrigger, mainMenuTrigger;
	public WinScreen(){
		try {
			winScreen = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("resources/winScreen.png"));
        	restartTexture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("resources/restartButton.png"));
			restartButton = new ButtonClass(Constants.restartButtonX, Constants.restartButtonY -25, restartTexture);
			menuTexture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("resources/mainMenuButton.png"));
			menuButton = new ButtonClass(Constants.mainMenuButtonX, Constants.mainMenuButtonY -25, menuTexture);
        	restartDownTexture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("resources/restartDownButton.png"));
			restartDownButton = new ButtonClass(Constants.restartButtonX, Constants.restartButtonY -25, restartDownTexture);
			menuDownTexture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("resources/mainMenuDownButton.png"));
			menuDownButton = new ButtonClass(Constants.mainMenuButtonX, Constants.mainMenuButtonY -25, menuDownTexture);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private boolean mouseReleased(){
	    if (Mouse.next()) {
	        if (Mouse.getEventButtonState()) {
	            if (Mouse.getEventButton() == 0) {
	                System.out.println("Left button pressed");
	            }
	        }
	        else{
		        if (Mouse.getEventButton() == 0) {
			           return true;
			    }
	        }
	    }
		return false;
	}
	
	private boolean releaseListener(int x, int y){
		if (Mouse.getX()>x && Mouse.getX()<(100+x) && Mouse.getY()<y && Mouse.getY()>(y-100) && mouseReleased()){
			return true;
		}
		else{
			return false;
		}
	}
	
	private boolean downListener(int x, int y){
		if (Mouse.getX()>x && Mouse.getX()<(100+x) && Mouse.getY()<y && Mouse.getY()>(y-100) && Mouse.isButtonDown(0)){
			return true;
		}
		else{
			return false;
		}
	}
	public void update(){
		if(releaseListener(Constants.restartButtonX, Constants.restartButtonY -25)){
	    	restartTrigger = true;
	    }
	    if(releaseListener(Constants.mainMenuButtonX, Constants.mainMenuButtonY -25)){
	    	mainMenuTrigger = true;
	    }
	}	
	public void render(){
		GL11.glEnable(GL11.GL_BLEND);
		Color.white.bind();
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, winScreen.getTextureID());
        GL11.glBegin(GL11.GL_QUADS);
        	GL11.glTexCoord2f(0,0);
	        GL11.glVertex2f(Constants.winScreenX, Constants.winScreenY);
        	GL11.glTexCoord2f(1,0);
		    GL11.glVertex2f(Constants.winScreenX+winScreen.getTextureWidth(), Constants.winScreenY);
        	GL11.glTexCoord2f(1,1);
		    GL11.glVertex2f(Constants.winScreenX+winScreen.getTextureWidth(),Constants.winScreenY-winScreen.getTextureHeight());
        	GL11.glTexCoord2f(0,1);
		    GL11.glVertex2f(Constants.winScreenX,Constants.winScreenY-winScreen.getTextureHeight());
		    GL11.glEnable(GL11.GL_TEXTURE_2D);
		    //Constants.font.drawString(centerTextX(this.lon), centerTextY(this.lat), "Resume", Color.black);
	    GL11.glEnd();
	    GL11.glDisable(GL11.GL_BLEND);
	    if(downListener(Constants.restartButtonX, Constants.restartButtonY -25)){
	    	restartDownButton.render();
	    }
	    else{
	    	restartButton.render();
	    }
	    if(downListener(Constants.mainMenuButtonX, Constants.mainMenuButtonY -25)){
	    	menuDownButton.render();
	    }
	    else{
	    	menuButton.render();
	    }
	    
	    
	    if(restartTrigger){
	    	ClientGUI temp = Game.parent;
	    	Display.destroy();
	    	new Game(temp, GameState.skinName, GameState.mapName);
	    	restartTrigger = false;
	    }
	    if(mainMenuTrigger){
	    	Game.trytoBreak = true;
	   // 	Game.parent.changeOpening();
	    	mainMenuTrigger = false;
	    }

	}

}

