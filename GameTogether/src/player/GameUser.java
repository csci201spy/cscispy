package player;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class GameUser  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String username;
	private String encryptedpassword;
	private String command;
	public String loginattempt = "login";
	public String signupattempt = "signup";
	public boolean loginsuccess;
	public boolean signupsuccess;
	
	public GameUser(String username, String password){
		this.username = username;
		this.encryptedpassword = encryptpassword(password);
	}

	
	public void setCommand(String incommand){
		this.command = incommand;
	}
	public String getCommand(){
		return command;
	}
	public String getName(){
		return username;
	}
	public String getEncryptedpassword(){
		return encryptedpassword;
	}
	
	private String encryptpassword(String password){
		//http://howtodoinjava.com/security/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/
		try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(password.getBytes());
            //Get the hash's bytes 
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            encryptedpassword = sb.toString();
            return encryptedpassword;
        } 
        catch (NoSuchAlgorithmException e) 
        {
            e.printStackTrace();
        }
		return "a";
	}
}
