package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import player.GameUser;

public class GameServerCommunicator extends Thread{
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private GameServerGUI servergui;
	private GameServerListener gsl;
	private GameSQLDriver sql;
	
	public GameServerCommunicator(GameServerListener gsl, Socket s,GameServerGUI parentgui,GameSQLDriver sql){
		servergui = parentgui;
		this.sql = sql;
		this.gsl = gsl;
		try {
			oos = new ObjectOutputStream(s.getOutputStream());
			ois = new ObjectInputStream(s.getInputStream());
			this.start();
		} catch (IOException ioe) {
			System.out.println("ioe: " + ioe.getMessage());
		}
		
	}

	public void run() {
		try {
			while(true) {
				GameUser attemptuser = null;
				attemptuser = (GameUser)ois.readObject();
				if(attemptuser.getCommand().equals(attemptuser.loginattempt)){
					verifylogin(attemptuser);
				}
				if(attemptuser.getCommand().equals(attemptuser.signupattempt)){
					verifysignup(attemptuser);
				}
				
			}
		} catch (ClassNotFoundException cnfe) {
			System.out.println("cnfe in run: " + cnfe.getMessage());
		} catch (IOException ioe) {
			System.out.println("ioe in run: " + ioe.getMessage());
			gsl.removeCommunicator(this);
			
		}
	}
	private void verifylogin(GameUser attemptuser){
		if(sql.exist(attemptuser.getName())){
			if(sql.verifylogin(attemptuser)){
				attemptuser.loginsuccess = true;
			}
		}
		try {
			oos.reset();
			oos.writeObject(attemptuser);
			oos.flush();
		} catch (IOException e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void verifysignup(GameUser attemptuser){
		if(!sql.exist(attemptuser.getName())){
			sql.addUserProfile(attemptuser);
			attemptuser.signupsuccess = true;
		}else{
			attemptuser.signupsuccess = false;
		}
		
		try {
			oos.reset();
			oos.writeObject(attemptuser);
			oos.flush();
		} catch (IOException e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
