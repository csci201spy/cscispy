package instance;
import static org.lwjgl.opengl.GL11.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Box2D extends AbstractEntity2D {
	protected String name;
	protected float x, y, size;
	protected Texture texture;
	
	public static enum Direction{
	      UP, DOWN, LEFT, RIGHT
	}
	
	public Box2D(String name, float x, float y, float size, String textureFile) {
		this.x = x;
		this.y = y;
		this.size = size;
		this.name = name;
		
		try {
			texture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(textureFile));
			
            System.out.println("Texture loaded: "+texture);
            System.out.println("Texture file: " + textureFile);
            System.out.println(">> Image width: "+texture.getImageWidth());
            System.out.println(">> Image height: "+texture.getImageHeight());
            System.out.println(">> Texture width: "+texture.getTextureWidth());
            System.out.println(">> Texture height: "+texture.getTextureHeight());
            System.out.println(">> Texture ID: "+texture.getTextureID());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Box2D() {
		this.x = this.y = this.size = 0;
	}
	
	// Constructor for hard-coding texture files
	public Box2D(String name, float x, float y, float size) {
		this.x = x;
		this.y = y;
		this.size = size;
		this.name = name;
		
		// Load texture in child class
		this.texture = null;
	}
	
	@Override
	public void setUp() {
		// We don't need anything here for a box
	}
	@Override
	public void destroy() {
		// We don't need anything here for a box
	} 
	
	public void update() {
		
	}
	
	public void render() {
		
	}
}