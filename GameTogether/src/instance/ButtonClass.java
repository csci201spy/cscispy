package instance;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

public class ButtonClass {
	private int lat;
	private int lon;
	private Texture tButton;
	
	public ButtonClass(int lon, int lat, Texture t){
		this.lon = lon;
		this.lat = lat;
		tButton = t;
		
	}
	
	private int centerTextX(int parentX){
		return parentX+25;
	}
	private int centerTextY(int parentY){
		return parentY+10;
	}
	
	public void render(){
		GL11.glEnable(GL11.GL_BLEND);
		Color.white.bind();
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, tButton.getTextureID());
        GL11.glBegin(GL11.GL_QUADS);
        	GL11.glTexCoord2f(0,0);
	        GL11.glVertex2f(lon, lat);
        	GL11.glTexCoord2f(1,0);
		    GL11.glVertex2f(lon+tButton.getImageWidth(),lat);
        	GL11.glTexCoord2f(1,1);
		    GL11.glVertex2f(lon+tButton.getImageWidth(),lat-tButton.getImageHeight());
        	GL11.glTexCoord2f(0,1);
		    GL11.glVertex2f(lon,lat-tButton.getImageHeight());
		    GL11.glEnable(GL11.GL_TEXTURE_2D);
		    //Constants.font.drawString(centerTextX(this.lon), centerTextY(this.lat), "Resume", Color.black);
	    GL11.glEnd();
	    GL11.glDisable(GL11.GL_BLEND);
		
	}

	
	
}
