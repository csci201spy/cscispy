package client;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import util.LeftArrowButton;
import util.MyButton;
import util.RightArrowButton;

public class MapSelectionGUI extends JPanel {
	private JLabel mapNameLabel;
	private JLabel mapImageLabel;
	private ImageIcon mapIcon;
	private LeftArrowButton leftButton;
	private RightArrowButton rightButton;
	private MyButton selectButton;
	private ClientGUI parent;
	private String tempMap;
	
	public MapSelectionGUI(ClientGUI parent) {
		super();
		this.parent = parent;
		createGUI();
	}
	
	private void createGUI() {
		setLayout(new BorderLayout());
		
		JLabel topLabel = new JLabel("Map");
		topLabel.setFont(new Font("Arial", Font.BOLD, 32));
		topLabel.setAlignmentX(CENTER_ALIGNMENT);
		
		mapNameLabel = new JLabel("Leavey Library");
		mapNameLabel.setFont(new Font("Arial", Font.PLAIN, 18));
		mapNameLabel.setAlignmentX(CENTER_ALIGNMENT);
		
		JPanel labelPanel = new JPanel();
		labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.Y_AXIS));
		labelPanel.add(topLabel);
		labelPanel.add(mapNameLabel);
		add(labelPanel, BorderLayout.NORTH);
				
		mapIcon = new ImageIcon("Resources/img/leavey.jpg");
		mapImageLabel = new JLabel(mapIcon);
		add(mapImageLabel, BorderLayout.CENTER);
		
		
		tempMap = parent.maps.get(parent.mapIndex);
		
		leftButton = new LeftArrowButton("");
		rightButton = new RightArrowButton("");
		refreshButtons(parent.mapIndex);
		
		leftButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				if(parent.mapIndex != 0) {
					parent.mapIndex -= 1;
					setMapShown(parent.maps.get(parent.mapIndex));
					refreshButtons(parent.mapIndex);
				}
			}
		});
		add(leftButton, BorderLayout.WEST);
			
		rightButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				if(parent.mapIndex != parent.maps.size()-1) {
					parent.mapIndex += 1;
					setMapShown(parent.maps.get(parent.mapIndex));
					refreshButtons(parent.mapIndex);
				}
			}
		});
		add(rightButton, BorderLayout.EAST);
		
		selectButton = new MyButton("SELECT");
		selectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				parent.setSelectedMap(tempMap);
				parent.startGamePanel.setMapLabel(tempMap);
				parent.mapPanel.setVisible(false);
				parent.add(parent.startGamePanel);
				parent.startGamePanel.setVisible(true);
			}
		});
		add(selectButton, BorderLayout.SOUTH);
	}
	
	private void refreshButtons(int mapIndex) {
		if(mapIndex == 0) {
			leftButton.setEnabled(false);
			rightButton.setEnabled(true);
		} else if (mapIndex == parent.maps.size()-1) {
			leftButton.setEnabled(true);
			rightButton.setEnabled(false);
		} else {
			leftButton.setEnabled(true);
			rightButton.setEnabled(true);
		}
	}
	
	private void setMapShown(String mapName) {
		if(mapName.equals("Kremlin") && parent.loggedIn == false) {
			mapIcon = new ImageIcon("Resources/img/" +  mapName + " Locked.jpg");
			selectButton.setEnabled(false);
		} else {
			mapIcon = new ImageIcon("Resources/img/" +  mapName + ".jpg");
			selectButton.setEnabled(true);
		}
		mapImageLabel.setIcon(mapIcon);
		mapNameLabel.setText(mapName);
		tempMap = mapName;
	}
}
