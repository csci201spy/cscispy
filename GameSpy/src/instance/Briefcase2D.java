package instance;

import java.awt.Point;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Briefcase2D extends Box2D {
	private Set<Point> interactableArea;
	private boolean pickedUp;
	
	public Briefcase2D(String name, float x, float y, float size) {
		this.x = x;
		this.y = y;
		this.size = size;
		this.name = name;
		pickedUp = false;
		
		interactableArea = new HashSet<Point>();
		
		try {
			texture = TextureLoader.getTexture(
					"PNG", ResourceLoader.getResourceAsStream(
							Constants.defaultBriefcaseTexture + Constants.pngFile));
			
            System.out.println("Texture loaded: "+texture);
            System.out.println("Texture file: " + Constants.defaultBriefcaseTexture);
            System.out.println(">> Image width: "+texture.getImageWidth());
            System.out.println(">> Image height: "+texture.getImageHeight());
            System.out.println(">> Texture width: "+texture.getTextureWidth());
            System.out.println(">> Texture height: "+texture.getTextureHeight());
            System.out.println(">> Texture ID: "+texture.getTextureID());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		width = texture.getTextureWidth()*size;
		height = texture.getTextureHeight()*size;
     
	    Point startingpoint; 
	    for(int i = (int)x; i < x+width; i++) {
	    	startingpoint = new Point(i,(int)y);
	    	for (int j = (int)y; j > y-height; j--) {
	    		if(!Game.briefcaseSet.contains(startingpoint)){
	    			interactableArea.add(startingpoint);
	            	startingpoint = new Point(i,j);
	    		}
	    	}
        }
	}
	
	public void update () {
		//check if there is player
		if (!pickedUp) {
	        for(Point point:Player2D.currentlocation){
	            if(interactableArea.contains(point)){
	                System.out.println("picked up briefcase");
	                pickedUp = true;
	                Game.isBriefcasePickedUp = true;
	                break;
	            }
	        }
		}
	}
	
	public void render () {
		if (!pickedUp) {
			GL11.glEnable(GL11.GL_BLEND);
	
			Color.white.bind();
	        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture.getTextureID());
	        
	        GL11.glBegin(GL11.GL_QUADS);
		        GL11.glTexCoord2f(0,0);
		        GL11.glVertex2f(x, y);
		        GL11.glTexCoord2f(1,0);
		        GL11.glVertex2f(x+(texture.getTextureWidth()*size), y);
		        GL11.glTexCoord2f(1,1);
		        GL11.glVertex2f(x+(texture.getTextureWidth()*size), y-(texture.getTextureHeight()*size));
		        GL11.glTexCoord2f(0,1);
		        GL11.glVertex2f(x, y-(texture.getTextureHeight()*size));
	        GL11.glEnd();
	        GL11.glDisable(GL11.GL_BLEND);	
		}
	}
}
