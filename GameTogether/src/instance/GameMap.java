package instance;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class GameMap {
	private FileReader fr;
	private BufferedReader br;
	
	ArrayList<Entity2D> entities;
	
	public GameMap(String mapFile) {
		fr = null;
		br = null;
	
		entities = new ArrayList<Entity2D>();
		try {
			fr = new FileReader(mapFile);
			br = new BufferedReader(fr);
			
			while (br.ready()) {
				String line = br.readLine();
				String[] args = line.split(" ");
				for (String s: args) {
					System.out.println(s);
				}
				
				if (args[0].equals("Wall")) {
					entities.add(new GameWall(
							Float.parseFloat(args[1]),
							Float.parseFloat(args[2]),
							Float.parseFloat(args[3]),
							Float.parseFloat(args[4]),
							Float.parseFloat(args[5])
						)
					);
				}
				else if (args[0].equals("Guard")) {
					Guard2D guard = new Guard2D(
						Float.parseFloat(args[1]),
						Float.parseFloat(args[2]),
						Float.parseFloat(args[3])
					);
					guard.setMoveArea(
						Float.parseFloat(args[4]),
						Float.parseFloat(args[5]),
						Float.parseFloat(args[6]),
						Float.parseFloat(args[7])
					);
					entities.add(guard);
				}
				else if (args[0].equals("Briefcase")) {
					Briefcase2D bc = new Briefcase2D(
						args[0],
						Float.parseFloat(args[1]),
						Float.parseFloat(args[2]),
						Float.parseFloat(args[3])
					);
					entities.add(bc);
				}
				else if (args[0].equals("Exit")) {
					Exit2D exit = new Exit2D(
						args[0],
						Float.parseFloat(args[1]),
						Float.parseFloat(args[2]),
						Float.parseFloat(args[3])
					);
					entities.add(exit);
				}
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			try {
				if (fr != null) fr.close();
				if (br != null) br.close();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}
}
