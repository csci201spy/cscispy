package client;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.Socket;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import player.GameUser;
import util.MyButton;

public class LoginScreenGUI extends JPanel {
	private Image titleImage;
	private JTextField usernameTF;
	private JTextField passwordTF;
	private MyButton signinButton;
	private MyButton registerButton;
	private ClientGUI parent;
	
	public LoginScreenGUI(ClientGUI parent) {
		super();
		this.parent = parent;
		parent.setSize(200,300);
		createGUI();
		setVisible(true);
	}
	
	private void createGUI() {
		setLayout(new BorderLayout());
		
		JLabel titleLabel = new JLabel("    Spy"); //get title image from resources folder
		titleLabel.setFont(new Font("Arial Black", Font.PLAIN, 40));
		titleLabel.setAlignmentX(CENTER_ALIGNMENT);
		
		JLabel usernameLabel = new JLabel(" Username: ");
		usernameTF = new JTextField();
		JLabel passwordLabel = new JLabel(" Password: ");
		passwordTF = new JTextField();
		
		signinButton = new MyButton("SIGN IN");
		signinButton.addActionListener(new SigninActionListener());
		registerButton = new MyButton("REGISTER");
		registerButton.addActionListener(new SignupListener());
		
		add(titleLabel,BorderLayout.NORTH);
		JPanel centerpanel = new JPanel();
		centerpanel.setLayout(new GridLayout(7,1));
		centerpanel.add(usernameLabel);
		centerpanel.add(usernameTF);
		centerpanel.add(passwordLabel);
		centerpanel.add(passwordTF);
		centerpanel.add(new JPanel());
		centerpanel.add(signinButton);
		centerpanel.add(registerButton);
		add(centerpanel,BorderLayout.CENTER);
		JPanel buttompanel = new JPanel();

		add(buttompanel,BorderLayout.SOUTH);

	}
	private class SigninActionListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(usernameTF.getText().length()==0 || passwordTF.getText().length()==0)return;
			Socket s = null;
			try {
				s = new Socket("localhost", parent.portnumber);
				GameUser attemptuser = new GameUser(usernameTF.getText(),passwordTF.getText());
				ClientCommunicator com = new ClientCommunicator(parent,s);
				com.sendloginattempt(attemptuser);
			} catch (IOException ioe) {
				System.out.println("ioe: " + ioe.getMessage());
				JOptionPane.showMessageDialog(parent,
					    "Server cannot be reached.\n Program is in offline mode",
					    "Log-in Failed",
					    JOptionPane.WARNING_MESSAGE);

			}
		}
		
	}
	private class SignupListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(usernameTF.getText().length()==0 || passwordTF.getText().length()==0)return;
			Socket s = null;
			try {
				s = new Socket("localhost", parent.portnumber);
				GameUser attemptuser = new GameUser(usernameTF.getText(),passwordTF.getText());
				ClientCommunicator com = new ClientCommunicator(parent,s);
				com.sendsignupattempt(attemptuser);
			} catch (IOException ioe) {
				System.out.println("ioe: " + ioe.getMessage());
				JOptionPane.showMessageDialog(parent,
					    "Server cannot be reached.\n Program is in offline mode",
					    "Log-in Failed",
					    JOptionPane.WARNING_MESSAGE);

			}
		}
		
	}
}
