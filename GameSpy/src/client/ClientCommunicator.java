package client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import player.GameUser;

public class ClientCommunicator extends Thread {
	private Socket s;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	//private GameManager manager;
	private ClientGUI clientGUI;
	
	//constructor TODO
	public ClientCommunicator(ClientGUI maingui,Socket s){
		this.s = s;
		this.clientGUI = maingui;
		boolean socketReady = initializeVariables();
		if (socketReady) {
			start();
		}
	}

	private boolean initializeVariables() {
		try {
			oos = new ObjectOutputStream(s.getOutputStream());
			ois = new ObjectInputStream(s.getInputStream());

		} catch (IOException ioe) {
			return false;
		}
		return true;
	}
	
	
	@Override
	public void run() {
		//TODO
		try {
			while(true) {
				// in case the server sends another factory to us
				GameUser newuser = (GameUser)ois.readObject();
				clientGUI.handleServerFeedback(newuser);
				
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
	}
	public void sendloginattempt(GameUser loginuser){
		loginuser.setCommand(loginuser.loginattempt);
		try {
			oos.reset();
			oos.writeObject(loginuser);
			oos.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void sendsignupattempt(GameUser signupuser){
		signupuser.setCommand(signupuser.signupattempt);
		try {
			oos.reset();
			oos.writeObject(signupuser);
			oos.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
