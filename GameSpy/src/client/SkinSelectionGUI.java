package client;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import util.LeftArrowButton;
import util.MyButton;
import util.RightArrowButton;

public class SkinSelectionGUI extends JPanel {
	private JLabel skinNameLabel;
	private ImageIcon skinIcon;
	private JLabel skinImageLabel;
	private LeftArrowButton leftButton;
	private RightArrowButton rightButton;
	private MyButton selectButton;
	private ClientGUI parent;
	private String tempSkin;
	
	public SkinSelectionGUI(ClientGUI parent) {
		super();
		this.parent = parent;
		createGUI();
	}
	
	private void createGUI() {
		setLayout(new BorderLayout());
		
		tempSkin = "default";
		
		JLabel topLabel = new JLabel("Skin");
		topLabel.setFont(new Font("Arial", Font.BOLD, 32));
		topLabel.setAlignmentX(CENTER_ALIGNMENT);
		
		skinNameLabel = new JLabel(parent.selectedSkin);
		skinNameLabel.setFont(new Font("Arial", Font.PLAIN, 18));
		skinNameLabel.setAlignmentX(CENTER_ALIGNMENT);
		
		JPanel labelPanel = new JPanel();
		labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.Y_AXIS));
		labelPanel.add(topLabel);
		labelPanel.add(skinNameLabel);
		add(labelPanel, BorderLayout.NORTH);
		
		ImageIcon myIcon = new ImageIcon("resources/skin_default_left.png");
		Image skin = myIcon.getImage();
		Image newSkin = skin.getScaledInstance(150, 250, Image.SCALE_SMOOTH);
		skinIcon = new ImageIcon(newSkin);
		skinImageLabel = new JLabel(skinIcon);
		add(skinImageLabel, BorderLayout.CENTER);
		
		leftButton = new LeftArrowButton("");
		leftButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				if(parent.skinIndex > 0) {
					parent.skinIndex -= 1;
					skinNameLabel.setText(parent.skins.get(parent.skinIndex));
					setSkinShown(parent.skins.get(parent.skinIndex));
					tempSkin = parent.skins.get(parent.skinIndex);
				}
			}
		});
		add(leftButton, BorderLayout.WEST);
		
		rightButton = new RightArrowButton("");
		rightButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				if(parent.skinIndex < parent.skins.size()-1) {
					parent.skinIndex += 1;
					skinNameLabel.setText(parent.skins.get(parent.skinIndex));
					setSkinShown(parent.skins.get(parent.skinIndex));
					tempSkin = parent.skins.get(parent.skinIndex);
				}
			}
		});
		add(rightButton, BorderLayout.EAST);
		
		selectButton = new MyButton("SELECT");
		selectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				parent.skinPanel.setVisible(false);
				parent.startGamePanel.setSkin(tempSkin);
				parent.add(parent.startGamePanel);
				parent.startGamePanel.setVisible(true);
			}
		});
		add(selectButton, BorderLayout.SOUTH);
	}
	
	private void setSkinShown(String skinName) {
		ImageIcon myIcon = new ImageIcon("resources/skin_" + skinName + "_left.png");
		Image skin = myIcon.getImage();
		Image newSkin = skin.getScaledInstance(150, 250, Image.SCALE_SMOOTH);
		skinIcon = new ImageIcon(newSkin);
		skinImageLabel.setIcon(skinIcon);
	}
}
