package instance;

import java.io.IOException;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import client.ClientGUI;

public class GameOverScreen {
	private static Texture restartTexture, menuTexture, restartDownTexture, menuDownTexture, goTexture;
	private ButtonClass restartButton, menuButton, restartDownButton, menuDownButton;
	private boolean restartTrigger, mainMenuTrigger;
	public GameOverScreen() {
        try {
        	goTexture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("resources/gameOver.png"));
        	restartTexture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("resources/restartButton.png"));
			restartButton = new ButtonClass(Constants.restartButtonX, Constants.restartButtonY, restartTexture);
			menuTexture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("resources/mainMenuButton.png"));
			menuButton = new ButtonClass(Constants.mainMenuButtonX, Constants.mainMenuButtonY, menuTexture);
        	restartDownTexture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("resources/restartDownButton.png"));
			restartDownButton = new ButtonClass(Constants.restartButtonX, Constants.restartButtonY, restartDownTexture);
			menuDownTexture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("resources/mainMenuDownButton.png"));
			menuDownButton = new ButtonClass(Constants.mainMenuButtonX, Constants.mainMenuButtonY, menuDownTexture);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        restartTrigger = false;
        mainMenuTrigger = false;
	}
	private boolean mouseReleased(){
	    if (Mouse.next()) {
	        if (Mouse.getEventButtonState()) {
	            if (Mouse.getEventButton() == 0) {
	                System.out.println("Left button pressed");
	            }
	        }
	        else{
		        if (Mouse.getEventButton() == 0) {
			           return true;
			    }
	        }
	    }
		return false;
	}
	
	private boolean releaseListener(int x, int y){
		if (Mouse.getX()>x && Mouse.getX()<(100+x) && Mouse.getY()<y && Mouse.getY()>(y-100) && mouseReleased()){
			return true;
		}
		else{
			return false;
		}
	}
	
	private boolean downListener(int x, int y){
		if (Mouse.getX()>x && Mouse.getX()<(100+x) && Mouse.getY()<y && Mouse.getY()>(y-100) && Mouse.isButtonDown(0)){
			return true;
		}
		else{
			return false;
		}
	}
	public void update(){
		if(releaseListener(Constants.restartButtonX, Constants.restartButtonY)){
	    	restartTrigger = true;
	    }
	    if(releaseListener(Constants.mainMenuButtonX, Constants.mainMenuButtonY)){
	    	mainMenuTrigger = true;
	    }
	}	
	public void render(){
		GL11.glEnable(GL11.GL_BLEND);
		Color.white.bind();
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, goTexture.getTextureID());
        GL11.glBegin(GL11.GL_QUADS);
        	GL11.glTexCoord2f(0,0);
	        GL11.glVertex2f(Constants.gameOverX, Constants.gameOverY);
        	GL11.glTexCoord2f(1,0);
		    GL11.glVertex2f(Constants.gameOverX+goTexture.getTextureWidth(), Constants.gameOverY);
        	GL11.glTexCoord2f(1,1);
		    GL11.glVertex2f(Constants.gameOverX+goTexture.getTextureWidth(),Constants.gameOverY-goTexture.getTextureHeight());
        	GL11.glTexCoord2f(0,1);
		    GL11.glVertex2f(Constants.gameOverX,Constants.gameOverY-goTexture.getTextureHeight());
		    GL11.glEnable(GL11.GL_TEXTURE_2D);
		    //Constants.font.drawString(centerTextX(this.lon), centerTextY(this.lat), "Resume", Color.black);
	    GL11.glEnd();
	    GL11.glDisable(GL11.GL_BLEND);
	    if(downListener(Constants.restartButtonX, Constants.restartButtonY)){
	    	restartDownButton.render();
	    }
	    else{
	    	restartButton.render();
	    }
	    if(downListener(Constants.mainMenuButtonX, Constants.mainMenuButtonY)){
	    	menuDownButton.render();
	    }
	    else{
	    	menuButton.render();
	    }
	    
	    
	    if(restartTrigger){
	    	ClientGUI temp = Game.parent;
	    	Game.trytoBreak = true;
	    	Display.destroy();
	    	new Game(temp, GameState.skinName, GameState.mapName);
	    	restartTrigger = false;
	    }
	    if(mainMenuTrigger){
	    	Display.destroy();
	    	Game.trytoBreak = true;
	    	Game.parent.changeOpening();
	    	mainMenuTrigger = false;
	    }
	}
}
