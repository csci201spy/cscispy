package instance;

import java.io.IOException;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class PauseMenu {
	private static Texture buttonTexture1, buttonTexture2, buttonTexture3, buttonTexture4, buttonDownTexture1, buttonDownTexture2,
	buttonDownTexture3, buttonDownTexture4, pauseTex;
	private ButtonClass b1, b2, b3, b4, bD1, bD2, bD3, bD4;
	private boolean resumeTrigger, changeMapTrigger, changeSkinTrigger, changeLoginTrigger;
	public PauseMenu(){
        try {
        	pauseTex = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("resources/pauseMenu.png"));
			buttonTexture1 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("resources/resumeButton.png"));
			b1 = new ButtonClass(Constants.resumeButtonX, Constants.resumeButtonY, buttonTexture1);
			buttonTexture2 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("resources/changeMButton.png"));
			b2 = new ButtonClass(Constants.changeMButotnX, Constants.changeMButtonY, buttonTexture2);
			buttonTexture3 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("resources/changeSButton.png"));
			b3 = new ButtonClass(Constants.changeSButtonX, Constants.changeSButtonY, buttonTexture3);
			buttonTexture4 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("resources/changeLButton.png"));
			b4 = new ButtonClass(Constants.changeLButtonX, Constants.changeLButtonY, buttonTexture4);
			buttonDownTexture1 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("resources/resumeDownButton.png"));
			bD1 = new ButtonClass(Constants.resumeButtonX, Constants.resumeButtonY, buttonDownTexture1);
			buttonDownTexture2 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("resources/changeMDownButton.png"));
			bD2 = new ButtonClass(Constants.changeMButotnX, Constants.changeMButtonY, buttonDownTexture2);
			buttonDownTexture3 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("resources/changeSDownButton.png"));
			bD3 = new ButtonClass(Constants.changeSButtonX, Constants.changeSButtonY, buttonDownTexture3);
			buttonDownTexture4 = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("resources/changeLDownButton.png"));
			bD4 = new ButtonClass(Constants.changeLButtonX, Constants.changeLButtonY, buttonDownTexture4);
	        resumeTrigger = false;
	        changeMapTrigger = false;
	        changeSkinTrigger = false;
	        changeLoginTrigger = false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private boolean mouseReleased(){
	    if (Mouse.getEventButton() > -1) {
	        if (Mouse.getEventButtonState()) {
	            if (Mouse.getEventButton() == 0) {
	                System.out.println("Left button pressed");
	            }
	        }
	        else{
		        if (Mouse.getEventButton() == 0) {
			           return true;
			    }
	        }
	    }
		return false;
	}
	
	private boolean releaseListener(int x, int y){
		if (Mouse.getX()>x && Mouse.getX()<(100+x) && Mouse.getY()<y && Mouse.getY()>(y-100) && mouseReleased()){
			return true;
		}
		else{
			return false;
		}
	}
	
	private boolean downListener(int x, int y){
		if (Mouse.getX()>x && Mouse.getX()<(100+x) && Mouse.getY()<y && Mouse.getY()>(y-100) && Mouse.isButtonDown(0)){
			return true;
		}
		else{
			return false;
		}
	}

	public void update(){
		Mouse.next();
		if(releaseListener(Constants.resumeButtonX, Constants.resumeButtonY)){
	    	resumeTrigger = true;
	    }
	    if(releaseListener(Constants.changeMButotnX, Constants.changeMButtonY)){
	    	changeMapTrigger = true;
	    }
	    if(releaseListener(Constants.changeSButtonX, Constants.changeSButtonY)){
	    	changeSkinTrigger = true;
	    }
	    if(releaseListener(Constants.changeLButtonX, Constants.changeLButtonY)){
	    	changeLoginTrigger = true;
	    }
	}
	
	public void render(){
		if(resumeTrigger){
			Game.isPaused = false; return;
		}

	    if(!resumeTrigger){
			GL11.glEnable(GL11.GL_BLEND);
			Color.white.bind();
	        GL11.glBindTexture(GL11.GL_TEXTURE_2D, pauseTex.getTextureID());
	        GL11.glBegin(GL11.GL_QUADS);
	        	GL11.glTexCoord2f(0,0);
		        GL11.glVertex2f(Constants.pauseMenuX, Constants.pauseMenuY);
	        	GL11.glTexCoord2f(1,0);
			    GL11.glVertex2f(Constants.pauseMenuX+pauseTex.getTextureWidth(), Constants.pauseMenuY);
	        	GL11.glTexCoord2f(1,1);
			    GL11.glVertex2f(Constants.pauseMenuX+pauseTex.getTextureWidth(),Constants.pauseMenuY-pauseTex.getTextureHeight());
	        	GL11.glTexCoord2f(0,1);
			    GL11.glVertex2f(Constants.pauseMenuX,Constants.pauseMenuY-pauseTex.getTextureHeight());
			    GL11.glEnable(GL11.GL_TEXTURE_2D);
			    //Constants.font.drawString(centerTextX(this.lon), centerTextY(this.lat), "Resume", Color.black);
		    GL11.glEnd();
		    GL11.glDisable(GL11.GL_BLEND);
		    if(downListener(Constants.resumeButtonX, Constants.resumeButtonY)){
		    	bD1.render();
		    }
		    else{
		    	b1.render();
		    }
		    if(downListener(Constants.changeMButotnX, Constants.changeMButtonY)){
		    	bD2.render();
		    }
		    else{
		    	b2.render();
		    }
		    if(downListener(Constants.changeSButtonX, Constants.changeSButtonY)){
		    	bD3.render();
		    }
		    else{
		    	b3.render();
		    }
		    if(downListener(Constants.changeLButtonX, Constants.changeLButtonY)){
		    	bD4.render();
		    }
		    else{
		    	b4.render();
		    }
			if(changeMapTrigger){
				//GOTO change map screen; 
				Game.changeMap = true;
				changeMapTrigger = false;
			}
			if(changeSkinTrigger){
				Game.changeSkin = true;
				changeSkinTrigger = false;
			}
			if(changeLoginTrigger){
				Game.Logout = true;
				changeLoginTrigger = false;
			}
	    }
	    else{
	    	return;
	    }
	}
}
