package server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import player.GameUser;

public class GameSQLDriver {

	private Connection con;
	private final static String selectName = "SELECT * FROM User WHERE username = ?";
	private final static String addUser = "INSERT INTO User(username,hashedpassword) VALUES(?,?)";
	public GameSQLDriver(){
		try {
			new com.mysql.jdbc.Driver();
			connect();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
	}
	public void connect(){
		try {
			con = DriverManager.getConnection("jdbc:mysql://localhost/GAMESPY?user=root&password=root&useSSL=false");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public boolean exist(String username){
		try {
			PreparedStatement ps = con.prepareStatement(selectName);
			ps.setString(1, username);
			ResultSet result = ps.executeQuery();
			if(result.next()){
				return true;
			}else{
				return false;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public boolean verifylogin(GameUser newuser){
		String username = newuser.getName();
		String hashedpassword = newuser.getEncryptedpassword();
		try {
			PreparedStatement ps = con.prepareStatement(selectName);
			ps.setString(1, username);
			ResultSet result = ps.executeQuery();
			if(result.next()){
				if(result.getString(3).equals(hashedpassword)){
					return true;
				}
			}else{
				return false;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public void addUserProfile(GameUser newuser){
			try {
				PreparedStatement ps = con.prepareStatement(addUser);
				ps.setString(1, newuser.getName());
				ps.setString(2, newuser.getEncryptedpassword());
				ps.executeUpdate();
			
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
	public void stop(){
		try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
