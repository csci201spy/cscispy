package client;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import util.MyButton;

public class OpeningScreenGUI extends JPanel {
	private Image titleImage;
	private MyButton signinButton;
	private MyButton playOfflineButton;
	private ClientGUI parent;
	
	public OpeningScreenGUI(ClientGUI parent) {
		super();
		this.parent = parent;
		createGUI();
		setVisible(true);
	}
	
	private void createGUI() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JLabel titleLabel = new JLabel("Spy"); //get title image from resources folder
		titleLabel.setFont(new Font("Arial Black", Font.PLAIN, 40));
		titleLabel.setAlignmentX(CENTER_ALIGNMENT);

		signinButton = new MyButton("SIGN IN");
		signinButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				//TODO
				//remove this panel and add signinPanel to ClientGUI
				parent.openingPanel.setVisible(false);
				parent.add(parent.signinPanel);
				parent.signinPanel.setVisible(true);
				
			}
		});
		signinButton.setAlignmentX(CENTER_ALIGNMENT);
		add(Box.createGlue());
		playOfflineButton = new MyButton("PLAY OFFLINE");
		playOfflineButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				//TODO
				//remove this panel and add startGamePanel to ClientGUI
				parent.openingPanel.setVisible(false);
				parent.setSize(500, 400);
				parent.add(parent.startGamePanel);
				parent.startGamePanel.setVisible(true);
				
			}
		});
		playOfflineButton.setAlignmentX(CENTER_ALIGNMENT);
		
		add(titleLabel);
		add(Box.createGlue());
		add(signinButton);
		add(Box.createGlue());
		add(playOfflineButton);
		add(Box.createGlue());
		add(Box.createGlue());


	}

}
