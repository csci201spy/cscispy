package util;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;

public class LeftArrowButton extends JButton {

	private static final long serialVersionUID = 1L;
	private Image defaultImage;
	private Image mouseoverImage;
	private boolean mouseover;
	String text;
	public LeftArrowButton(String inputtext){
		super(inputtext);
		text = inputtext;
		setOpaque(false);
		setContentAreaFilled(false);
		setBorderPainted(false);
		mouseover = false;
		this.addMouseListener(new MouseAdapter(){
			public void mouseEntered(MouseEvent evt)
            {
				mouseover = true;
				repaint();
            }
            public void mouseExited(MouseEvent evt)
            {
            	mouseover = false;
				repaint();
            }
		});
	}
	
	public void setNewText(String newtext){
		this.setText(newtext);
		text = newtext;
		Graphics g = this.getGraphics();
		int buttonwidth = this.getWidth();
		int buttonheight = this.getHeight();
		int strwidth =  g.getFontMetrics().stringWidth(text);
		g.setColor(Color.white);
		g.drawString(text, (buttonwidth-strwidth)/2, buttonheight-9);
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		try {
			defaultImage = ImageIO.read(new File("resources/left-arrow-gray.png"));	
			mouseoverImage = ImageIO.read(new File("resources/left-arrow.png"));
			int buttonwidth = this.getWidth();
			int buttonheight = this.getHeight();
			int strwidth =  g.getFontMetrics().stringWidth(text);
			if(!mouseover)g.drawImage(defaultImage, 10, 10, this.getWidth()-10, this.getHeight()-10,null);
			else g.drawImage(mouseoverImage, 10, 10, this.getWidth()-10, this.getHeight()-10,null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
