package util;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;

public class MyButton extends JButton {

	private static final long serialVersionUID = 1L;
	private Image defaultImage;
	private Image mouseoverImage;
	private Image disabledImage;
	private boolean mouseover;
	String text;
	public MyButton(String inputtext){
		super(inputtext);
		text = inputtext;
		
		mouseover = false;
		this.addMouseListener(new MouseAdapter(){
			public void mouseEntered(MouseEvent evt)
            {
				mouseover = true;
				repaint();
            }
            public void mouseExited(MouseEvent evt)
            {
            	mouseover = false;
				repaint();
            }
		});
	}
	
	public void setNewText(String newtext){
		this.setText(newtext);
		text = newtext;
		Graphics g = this.getGraphics();
		int buttonwidth = this.getWidth();
		int buttonheight = this.getHeight();
		int strwidth =  g.getFontMetrics().stringWidth(text);
		g.setColor(Color.white);
		g.drawString(text, (buttonwidth-strwidth)/2, buttonheight-9);
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		try {
			defaultImage = ImageIO.read(new File("Resources/img/button.png"));	
			mouseoverImage = ImageIO.read(new File("Resources/img/button_hover.png"));
			disabledImage = ImageIO.read(new File("Resources/img/button_disabled.png"));
			int buttonwidth = this.getWidth();
			int buttonheight = this.getHeight();
			int strwidth =  g.getFontMetrics().stringWidth(text);
			
			if(isEnabled()) {
				if(!mouseover )g.drawImage(defaultImage, 0, 0, this.getWidth(), this.getHeight(),null);
				else g.drawImage(mouseoverImage, 0, 0, this.getWidth(), this.getHeight(),null);
			} else {
				g.drawImage(disabledImage, 0, 0, this.getWidth(), this.getHeight(), null);
			}
			
			g.setColor(Color.white);
			g.drawString(text, (buttonwidth-strwidth)/2, buttonheight-9);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
