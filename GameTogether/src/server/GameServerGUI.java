package server;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import util.MyButton;

public class GameServerGUI extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int portnumber;
	private JTextArea textarea;
	private JScrollPane textareapane;
	private MyButton ListenButton;
	private GameServerListener serverlistener;
	public GameServerGUI(int portnumber){
		super("server");
		this.portnumber = portnumber;
		setSize(300, 400);
		setLocation(100,50);
		initializeVariables();
		createGUI();
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
	private void initializeVariables(){
		textarea = new JTextArea();
		textarea.setEditable(false);
		textareapane = new JScrollPane(textarea);
		textareapane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		ListenButton = new MyButton("Start");

		ListenButton.addActionListener(new ServerActionListener());
	}
	private void createGUI(){
		this.add(textareapane, BorderLayout.CENTER);
		this.add(ListenButton,BorderLayout.SOUTH);
	}
	private class ServerActionListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(ListenButton.getText().equals("Start")){
				ListenButton.setNewText("Stop");
				textarea.append("Server started on port: "+portnumber+'\n');
				listenForConnections();
			}
			else{
				ListenButton.setNewText("Start");
				textarea.append("Server stopped."+'\n');	
			}
		}
		
	}
	private void listenForConnections() {
		serverlistener = new GameServerListener(portnumber,this);
		serverlistener.start();
		
	}
}
