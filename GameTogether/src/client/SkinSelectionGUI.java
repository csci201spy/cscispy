package client;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import util.MyButton;

public class SkinSelectionGUI extends JPanel {
	private JLabel skinNameLabel;
	private Image skinImage;
	private MyButton leftButton;
	private MyButton rightButton;
	private MyButton selectButton;
	private ClientGUI parent;
	
	public SkinSelectionGUI(ClientGUI parent) {
		super();
		this.parent = parent;
		createGUI();
	}
	
	private void createGUI() {
		setLayout(new BorderLayout());
		
		JLabel topLabel = new JLabel("Skin");
		topLabel.setFont(new Font("Arial", Font.BOLD, 32));
		topLabel.setAlignmentX(CENTER_ALIGNMENT);
		
		skinNameLabel = new JLabel("Default Skin");
		skinNameLabel.setFont(new Font("Arial", Font.PLAIN, 18));
		skinNameLabel.setAlignmentX(CENTER_ALIGNMENT);
		
		JPanel labelPanel = new JPanel();
		labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.Y_AXIS));
		labelPanel.add(topLabel);
		labelPanel.add(skinNameLabel);
		add(labelPanel, BorderLayout.NORTH);
		
		ImageIcon skinIcon = new ImageIcon("resources/default_skin_hires.png");
		Image skin = skinIcon.getImage();
		Image newSkin = skin.getScaledInstance(150, 250, Image.SCALE_SMOOTH);
		skinIcon = new ImageIcon(newSkin);
		JLabel skinLabel = new JLabel(skinIcon);
		add(skinLabel, BorderLayout.CENTER);
		
		leftButton = new MyButton("LEFT");
		leftButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				//TODO
				//if enabled, move left one index in skins array
				//if new index is 0, disable left button
			}
		});
		add(leftButton, BorderLayout.WEST);
		
		rightButton = new MyButton("RIGHT");
		rightButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				//TODO
				//if enabled, move right one index in skins array
				//if new index is skins.size()-1, disable right button
			}
		});
		add(rightButton, BorderLayout.EAST);
		
		selectButton = new MyButton("SELECT");
		selectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				//selectSkin();
				parent.skinPanel.setVisible(false);
				parent.add(parent.startGamePanel);
				//parent.startGamePanel.setSkin();
				parent.startGamePanel.setVisible(true);
			}
		});
		add(selectButton, BorderLayout.SOUTH);
	}
	
	private void loadSkin(String skinName) {
		//TODO
	}
	
	private void selectSkin(String skinName) {
		//TODO
	}
}
