package instance;

import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.glClear;

import java.awt.Point;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Guard2D extends Box2D {
	private Point begin;
	private Point end;
	private Boolean moveHorizontally;
	private Direction direction;
	private float speed;
    private Set<Point> sightblock;
    private Texture rightTexture, leftTexture;
    private int narrowback;
    private int narrowvision;
    private int narrowvision2;


	
	public Guard2D(float x, float y, float size) {
		super("Guard", x, y, size);
		speed = (float) 1;
		narrowback = 8;
		narrowvision = 4;
		narrowvision2 = 15;

		
		try {
			rightTexture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(Constants.defaultGuardRightTexture + Constants.pngFile));
			leftTexture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(Constants.defaultGuardLeftTexture + Constants.pngFile));
			texture = rightTexture;
			
            System.out.println("Texture loaded: "+texture);
            System.out.println("Texture file: " + Constants.defaultGuardRightTexture + Constants.pngFile);
            System.out.println(">> Image width: "+texture.getImageWidth());
            System.out.println(">> Image height: "+texture.getImageHeight());
            System.out.println(">> Texture width: "+texture.getTextureWidth());
            System.out.println(">> Texture height: "+texture.getTextureHeight());
            System.out.println(">> Texture ID: "+texture.getTextureID());
		} catch (IOException e) {
			e.printStackTrace();
		}
		width = texture.getTextureWidth()*size;
		height = texture.getTextureHeight()*size;
        sightblock = new HashSet<Point>();
	}
	
	public void setMoveArea(float xbegin, float ybegin, float xend, float yend){
		begin = new Point();
		begin.setLocation(xbegin, ybegin);
		end = new Point();
		end.setLocation(xend, yend);
		if(xbegin == xend){
			moveHorizontally = false;
			direction = Direction.UP;
		}
		else if(ybegin == yend){
			moveHorizontally = true;
			direction = Direction.RIGHT;
		}
		ChangeImageDirection();
	}
	
	public void update() {
        //check if there is player
		if(isclose()){
			
			for(Point point:Player2D.currentlocation){
	            if(sightblock.contains(point)){
	            	System.out.println("lost: "+x+" "+y);
	            	System.out.println(Game.player.getX()+" "+Game.player.getY());

	            	Game.gamelost = true;
	                break;
	            }
	        }
			
		}
        sightblock = new HashSet<Point>();

        
        
		if(moveHorizontally== null)return;
			
		if(moveHorizontally){
			if(x>=begin.getX()&&x<=end.getX()&& direction == Direction.RIGHT){
				x+=speed;if(isclose()){createSightBlock();}return;
			}
			if(x>=end.getX() && direction == Direction.RIGHT){
				x-=speed; direction = Direction.LEFT;ChangeImageDirection();if(isclose()){createSightBlock();}return;
			}
			if(x>begin.getX()&&direction == Direction.LEFT){
				x-=speed;if(isclose()){createSightBlock();}return;
			}
			if(x<=begin.getX()&&direction == Direction.LEFT){
				x+=speed;direction= Direction.RIGHT; ChangeImageDirection();if(isclose()){createSightBlock();}return;
			}
		}else{
			if(y>=begin.getY()&&y<=end.getY()&& direction == Direction.UP){
				y+=speed;if(isclose()){createSightBlock();}return;
			}
			if(y>=end.getY() && direction == Direction.UP){
				y-=speed; direction = Direction.DOWN;ChangeImageDirection();if(isclose()){createSightBlock();}return;
			}
			if(y>begin.getY()&&direction == Direction.DOWN){
				y-=speed;if(isclose()){createSightBlock();}return;
			}
			if(y<=begin.getY()&&direction == Direction.DOWN){
				y+=speed;direction= Direction.UP; ChangeImageDirection();if(isclose()){createSightBlock();}return;
			}
			
		}
		
	}
	private boolean isclose(){
		float verticaldistance = Math.abs(Game.player.getY()-y);
		float horizontaldistance = Math.abs(Game.player.getX()-x);

		boolean closevertically =((!moveHorizontally)&&(horizontaldistance<30)&&(verticaldistance<300));
		boolean closehorizontally = (moveHorizontally&& (horizontaldistance<300)&&(verticaldistance<30));
	

		if(closehorizontally||closevertically){
			return true;
		}
		return false;
	}
    
    private void createSightBlock(){
        sightblock = new HashSet<Point>();
        
        if(direction == Direction.LEFT){
            int tempx = (int)x-narrowback;
            int tempy = (int)y;
            //System.out.println("------------");
            
            Point startingpoint = new Point(tempx,tempy);
            for(tempy=(int)y-narrowvision;tempy>y-height+narrowvision;tempy--){
                while(!Game.invalidset.contains(startingpoint)){
                    sightblock.add(new Point(tempx,tempy));
                    tempx =tempx-1;
                    startingpoint = new Point(tempx,tempy);
                    //	System.out.println(tempx+" "+tempy);
                }
                tempx =(int)x-narrowback;
                startingpoint = new Point(tempx,tempy);
            }
            
        }
        if(direction == Direction.RIGHT){
            int tempx = (int)x+narrowback;
            int tempy = (int)y;
            Point startingpoint = new Point(tempx,tempy);

            for(tempy=(int)y-narrowvision;tempy>y-height+narrowvision;tempy--){

                while(!Game.invalidset.contains(startingpoint)){
                    sightblock.add(new Point(tempx,tempy));
                    tempx =tempx+1;
                    startingpoint = new Point(tempx,tempy);
                }
                tempx =(int)x+narrowback;
                startingpoint = new Point(tempx,tempy);
            }
            
        }
        if(direction == Direction.UP){
            int tempx = (int)x;
            int tempy = (int)y+narrowback;
            //System.out.println("------------");
            
            Point startingpoint = new Point(tempx,tempy);
            for(tempx=(int)x+narrowvision2;tempx<x+width-narrowvision2;tempx++){
                while(!Game.invalidset.contains(startingpoint)){
                    sightblock.add(new Point(tempx,tempy));
                    tempy =tempy+1;
                    startingpoint = new Point(tempx,tempy);
                    //	System.out.println(tempx+" "+tempy);
                }
                tempy =(int)y+narrowback;
                startingpoint = new Point(tempx,tempy);
            }
            
        }
        if(direction == Direction.DOWN){
            int tempx = (int)x;
            int tempy = (int)y-narrowback;
            //System.out.println("------------");
            
            Point startingpoint = new Point(tempx,tempy);
            for(tempx=(int)x+narrowvision2;tempx<x+width-narrowvision2;tempx++){
                while(!Game.invalidset.contains(startingpoint)){
                    sightblock.add(new Point(tempx,tempy));
                    tempy =tempy-1;
                    startingpoint = new Point(tempx,tempy);
                    //	System.out.println(tempx+" "+tempy);
                }
                tempy =(int)y-narrowback;
                startingpoint = new Point(tempx,tempy);
            }
            
        }
    }

	
	private void ChangeImageDirection(){
		if(direction == Direction.RIGHT){
			texture = rightTexture;
		}
		else if(direction == Direction.LEFT){
			texture = leftTexture;
		}

	}
	
	public void render() {
		GL11.glEnable(GL11.GL_BLEND);
        
		Color.white.bind();
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture.getTextureID());
        GL11.glBegin(GL11.GL_QUADS);
	        GL11.glTexCoord2f(0,0);
	        GL11.glVertex2f(x, y);
	        GL11.glTexCoord2f(1,0);
	        GL11.glVertex2f(x+(texture.getTextureWidth()*size), y);
	        GL11.glTexCoord2f(1,1);
	        GL11.glVertex2f(x+(texture.getTextureWidth()*size), y-(texture.getTextureHeight()*size));
	        GL11.glTexCoord2f(0,1);
	        GL11.glVertex2f(x, y-(texture.getTextureHeight()*size));
        GL11.glEnd();
        GL11.glDisable(GL11.GL_BLEND);
	}
}
