package instance;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

import client.ClientGUI;

import static org.lwjgl.opengl.GL11.*;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * If this application shows a blank and responsive window
 * and doesn't throw any errors, you know you have installed lwjgl
 * correctly.
 * @author Oskar Veerhoek
 */
public class Game {
	ArrayList<Entity2D> gameEntities;
	GameMap gameMap;
	static Player2D player;
	static Set<Point> invalidset;
	static Set<Point> briefcaseSet;
	static Set<Point> exitSet;
	
	public static boolean gamewin;
	public static boolean gamelost;
	public static boolean isPaused;
	public static boolean isBriefcasePickedUp;
	public static boolean changeMap;
	public static boolean changeSkin;
	public static boolean Logout;
	public static boolean trytoBreak;
	public static boolean mainmenutrigger;
	private String skin, map;


	private PauseMenu pauseMenu;
	private WinScreen ws;
    GameOverScreen gos;
    public static ClientGUI parent;
	
	public Game(ClientGUI parent, String skin, String map) {
		// Constructor for the game instance
		// will eventually take in a GameState style object that
		// will contain the map and skin settings to initialize
		this.parent = parent;
		GameState.resetState();
		GameState.skinName = skin;
		GameState.mapName = map;
		this.skin = skin;
		this.map = map;
		gameEntities = new ArrayList<Entity2D>();
		invalidset = new HashSet<Point>();
		briefcaseSet = new HashSet<Point>();
		exitSet = new HashSet<Point>();
		gamewin = false;
		gamelost = false;
		isPaused = false;
		trytoBreak = false;
		isBriefcasePickedUp = false;
		changeMap = false;
		changeSkin = false;
		Logout = false;
		mainmenutrigger =false;
		try {
			// Sets the width of the display to 640 and the height to 480
			Display.setDisplayMode(new DisplayMode(Constants.windowWidth, Constants.windowHeight));
			// Sets the title of the display to "Episode 2 - Display"
			Display.setTitle("Spy");
			// Creates and shows the display
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			Display.destroy();
			System.exit(1);
		}
		
		gameMap = new GameMap(map);
		gameEntities.addAll(gameMap.entities);
		
		player.setSkin(skin);
		gameEntities.add(player);
				
	
		// Enter the state that is required for modify the projection. Note that, in contrary to Java2D, the vertex
		// coordinate system does not have to be equal to the window coordinate space. The invocation to glOrtho creates
		// a 2D vertex coordinate system like this:
		// Upper-Left:  (0,480)   Upper-Right:  (640,480)
		// Bottom-Left: (0,0) Bottom-Right: (640,0)
		// If you skip the glOrtho method invocation, the default 2D projection coordinate space will be like this:
		// Upper-Left:  (-1,+1) Upper-Right:  (+1,+1)
		// Bottom-Left: (-1,-1) Bottom-Right: (+1,-1)
	
		GL11.glEnable(GL11.GL_TEXTURE_2D);     
	
		GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
		GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
	
		//GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);          
	 
		// enable alpha blending
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
	  
        GL11.glViewport(0,0,Constants.windowWidth,Constants.windowHeight);
        
        //GL11.glMatrixMode(GL11.GL_MODELVIEW);
        //GL11.glMatrixMode(GL11.GL_PROJECTION);
        
        GL11.glLoadIdentity();
        GL11.glOrtho(0, Constants.windowWidth, 0, Constants.windowHeight, 1, -1);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        
        // While we aren't pressing the red button on the display
        while (!Display.isCloseRequested()&&!gamewin&&!gamelost) {
	      	// Clear the 2D contents of the window.
		  	// Before game loop
        	if(changeMap){
        		parent.changeMap();
    			break;
    		}
        	if(changeSkin){
        		parent.changeSkin();
    			break;
    		}
        	if(Logout){
        		parent.Logout();
    			break;
        	}
        	checkstate();
    		glClear(GL_COLOR_BUFFER_BIT);  
    		if(isPaused){
    			pauseMenu.update();
    			pauseMenu.render();
    		}
    		if(!isPaused){
    			for(Entity2D e: gameEntities) {
            		e.update();
            	}
            	
            	for(Entity2D e: gameEntities) {
            		e.render();
            	}
    		}
    		
        	
        	
      
	  		// Update the contents of the display and check for input.
	  		Display.update();
	
	  		// Wait until we reach 60 frames-per-second.
	  		Display.sync(60);
	  	}
        if(gamewin){
        	ws =new WinScreen();
        	while(!Display.isCloseRequested()){
        		ws.update();
        	    if(trytoBreak) break;
        		ws.render();
        		// Update the contents of the display and check for input.
    	  		Display.update();
    	  		// Wait until we reach 60 frames-per-second.
    	  		Display.sync(60);
    		}
    		
		}
        if(gamelost){
    		gos = new GameOverScreen();
    		while(!Display.isCloseRequested()){
    			gos.update();
        	    gos.render();
        	    if(trytoBreak)break;
        	 // Update the contents of the display and check for input.
    	  		Display.update();
    	  		// Wait until we reach 60 frames-per-second.
    	  		Display.sync(60);
    		}
    		
    	}   
     //   while(!Display.isCloseRequested())
       
		Display.destroy();
		if(mainmenutrigger)parent.changeOpening();
	//	System.exit(0);
	}
	
	private void checkstate(){
		if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)){
			pauseMenu = new PauseMenu();
			isPaused = true;
		}
	}
}


