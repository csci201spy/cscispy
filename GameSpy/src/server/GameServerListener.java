package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

public class GameServerListener extends Thread{
	private ServerSocket ss;
	private int portnumber;
	private GameServerGUI servergui;
	private Vector<GameServerCommunicator> communicators;
//	private OfficeSQLDriver officesql;
	private GameSQLDriver sql;
	public GameServerListener(int port, GameServerGUI parentgui){
		portnumber = port;
		servergui = parentgui;
		communicators = new Vector<GameServerCommunicator>();
		sql = new GameSQLDriver();
		ss = null;
		try {
			ss = new ServerSocket(portnumber);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public void run() {
		try {
			
			while (true){			
				Socket s = ss.accept();
				GameServerCommunicator communicator = new GameServerCommunicator(this,s,servergui,sql);
				communicators.add(communicator);
			}
		} catch (IOException ioe) {
			System.out.println("ioe: " + ioe.getMessage());
		} finally {
			if (ss != null) {
				try {
					ss.close();
				} catch (IOException ioe) {
					System.out.println("ioe closing ss: " + ioe.getMessage());
				}
			}
		}
	}
	public void stopconnection(){
		if (ss != null) {
			try {
				ss.close();
			} catch (IOException ioe) {
				System.out.println("ioe closing ss: " + ioe.getMessage());
			}
		}
		
	}
	public void removeCommunicator(GameServerCommunicator sc){
		communicators.remove(sc);
	}
}
